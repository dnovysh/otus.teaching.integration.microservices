﻿namespace Otus.Teaching.Integration.Microservices.CreditApplicationConsumer.Host
{
    public class IntegrationEndpointSettings
    {
        public string MessageBrokerEndpoint { get; set; }
    }
}