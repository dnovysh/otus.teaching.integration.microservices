using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Repositories;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.DataAccess.Repositories;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddScoped<ICreditApplicationRepository, CreditApplicationCrmRepository>();
                    services.AddScoped<ICreditApplicationFileSerializer, CreditApplicationFileXmlSerializer>();
                    services.AddScoped<ICreditApplicationExporter, SharedDirectoryCustomerExporter>();
                    services.AddHostedService<ExportWorker>();
                });
    }
}