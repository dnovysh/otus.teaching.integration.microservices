﻿using System;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto
{
    public class CreditApplicationExportDto
    {
        public Guid Id { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public string Channel { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string Status { get; set; }
        
        public decimal Sum { get; set; }
    }
}