﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export;
using SimpleImpersonation;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export
{
    public class SharedDirectoryCustomerExporter
        : ICreditApplicationExporter
    {
        private readonly ICreditApplicationFileSerializer _creditApplicationFileSerializer;
        private readonly SharedDirectoryExportSettings _sharedDirectoryExportSettings;

        public SharedDirectoryCustomerExporter(ICreditApplicationFileSerializer creditApplicationFileSerializer, IOptions<SharedDirectoryExportSettings> exportSettings)
        {
            _creditApplicationFileSerializer = creditApplicationFileSerializer;
            _sharedDirectoryExportSettings = exportSettings.Value;
        }

        public Task ExportAsync(List<CreditApplicationExportDto> applications)
        {
            _creditApplicationFileSerializer.SerializeToFile(applications);

            if (!File.Exists(_sharedDirectoryExportSettings.FileName))
                throw new FileNotFoundException("Файл для экспорта не был сформирован");
            
            //Пользователь, под которым запущен сервис может не иметь доступа к сетевому ресурсу, поэтом используем
            //специального пользователя, о котором договорились
            var credentials = new UserCredentials(_sharedDirectoryExportSettings.ShareUserDomain,
                _sharedDirectoryExportSettings.ShareUserName, _sharedDirectoryExportSettings.ShareUserPassword);
            Impersonation.RunAsUser(credentials, LogonType.NewCredentials,
                () =>
                {
                    File.Copy(_sharedDirectoryExportSettings.FileName,
                        _sharedDirectoryExportSettings.ShareFileName);
                });

            return Task.CompletedTask;
        }
    }
}