﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Extensions.Options;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Export;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export
{
    public class CreditApplicationFileXmlSerializer
        : ICreditApplicationFileSerializer
    {
        private readonly SharedDirectoryExportSettings _sharedDirectoryExportSettings;
        
        public CreditApplicationFileXmlSerializer(IOptions<SharedDirectoryExportSettings> exportSettings)
        {
            _sharedDirectoryExportSettings = exportSettings.Value;
        }
        
        public void SerializeToFile(List<CreditApplicationExportDto> customers)
        {
            using var stream = File.Create(_sharedDirectoryExportSettings.FileName);
            new XmlSerializer(typeof(CreditApplicationExportListDto)).Serialize(stream, new CreditApplicationExportListDto()
            {
                CreditApplications = customers
            });
        }
    }
}