﻿namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export
{
    public class SharedDirectoryExportSettings
    {
        public string FileName { get; set; }

        public string ShareFileName { get; set; }
        
        public string ShareUserDomain { get; set; }

        public string ShareUserName { get; set; }

        public string ShareUserPassword { get; set; }
        
    }
}