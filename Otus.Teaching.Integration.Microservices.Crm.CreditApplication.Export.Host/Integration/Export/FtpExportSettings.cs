﻿namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Integration.Export
{
    public class FtpExportSettings
    {
        public string FileName { get; set; }

        public string FtpUrl { get; set; }

        public string FtpUser { get; set; }

        public string FtpPassword { get; set; }
        
    }
}