﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Dto;
using Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.Core.Repositories;

namespace Otus.Teaching.Integration.Microservices.Crm.CreditApplication.Export.DataAccess.Repositories
{
    public class CreditApplicationCrmRepository
        : ICreditApplicationRepository
    {
        public async Task<List<CreditApplicationExportDto>> GetAllForExportAsync()
        {
            var credits = await Task.FromResult(new List<CreditApplicationExportDto>()
            {
                new CreditApplicationExportDto()
                {
                    Id = Guid.NewGuid(),
                    Channel = "Email",
                    Status = "New",
                    Sum = 121331,
                    CreatedDate = DateTime.Now,
                    CustomerId = Guid.NewGuid()
                }
            });

            return credits;
        }
    }
}