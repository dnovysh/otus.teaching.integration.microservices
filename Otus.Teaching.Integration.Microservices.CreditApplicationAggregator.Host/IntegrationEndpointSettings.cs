﻿namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host
{
    public class IntegrationEndpointSettings
    {
        public string MessageBrokerEndpoint { get; set; }
    }
}