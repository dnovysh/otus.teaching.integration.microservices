﻿using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Integration.Microservices.Contract;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Integration.Producers
{
    public class CreateNewCreditApplicationProducer
        : ICreateNewCreditApplicationProducer
    {
        private readonly ISendEndpointProvider _sendEndpointProvider;
        private readonly ILogger<CreateNewCreditApplicationProducer> _logger;

        public CreateNewCreditApplicationProducer(ISendEndpointProvider sendEndpointProvider, ILogger<CreateNewCreditApplicationProducer> logger)
        {
            _sendEndpointProvider = sendEndpointProvider;
            _logger = logger;
        }
        
        public async Task CreateNewCreditApplication(CreditApplicationImportDto application)
        {
            _logger.LogInformation($"Отправляем команду на создание кредитной заявки с Id {application.Id} в шину...");
            
            await _sendEndpointProvider.Send<CreateNewCreditApplicationContract>(application);
            
            _logger.LogInformation($"Отправили команду на создание кредитной заявки с Id {application.Id} в шину");
        }
    }
}