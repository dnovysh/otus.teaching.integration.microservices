﻿﻿namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication
{
    public enum AcquisitionChannel
    {
        Street = 1,
        
        Email =2,
        
        Sms = 3
    }

}
