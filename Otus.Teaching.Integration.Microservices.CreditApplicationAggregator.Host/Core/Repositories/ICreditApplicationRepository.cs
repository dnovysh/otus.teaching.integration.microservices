﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.DataAccess.Repositories.Abstractions
{
    public interface ICreditApplicationRepository
    {
        Task<IEnumerable<CreditApplication>> GetAllAsync();

        Task<CreditApplication> GetByIdAsync(Guid id);

        Task AddAsync(CreditApplication entity);
        
        Task UpdateAsync(CreditApplication entity);

        Task DeleteAsync(Guid entityId);
    }
}