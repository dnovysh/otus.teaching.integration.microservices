﻿using System;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    ///<summary>
    /// Данные изменения заявки
    /// </summary>
    /// <example>
    ///{
    ///    "id": de691462-c91e-4342-83c1-2a98b572d075,
    ///    "customerId": "1fb6f3ef-fd58-4df9-bb29-0bc6f9b57241",
    ///    "channel": 2,
    ///    "Sum": 10000.12
    ///}
    /// </example>
    public class EditCreditApplicationDto
    {
        public Guid Id { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public AcquisitionChannel Channel { get; set; }
        public CreditApplicationStatus Status { get; set; }
        
        public decimal Sum { get; set; }
    }
}
