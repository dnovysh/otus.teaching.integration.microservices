﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    [XmlRoot("CreditApplications")]
    public class CreditApplicationImportListDto
    {
        public List<CreditApplicationImportDto> CreditApplications { get; set; }
    }
}