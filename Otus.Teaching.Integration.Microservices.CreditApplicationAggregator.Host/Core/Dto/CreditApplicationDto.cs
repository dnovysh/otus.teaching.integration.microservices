﻿using System;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    public class CreditApplicationDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }
	    
        /// <summary>
        /// Полное имя клиента
        /// </summary>
        public Guid CustomerId { get; set; }
	
        /// <summary>
        /// Канал привлечения (интернет-реклама, реклама на улице и т.д.)
        /// </summary>
        public AcquisitionChannel Channel { get; set; }
	
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }
	
        /// <summary>
        /// Статус
        /// </summary>
        public CreditApplicationStatus Status { get; set; }

        public decimal Sum { get; set; }
    }
}

