﻿using System;
using Otus.Teaching.Integration.Microservices.Contract;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    public class CreditApplicationImportDto
        : CreateNewCreditApplicationContract
    {
        public Guid Id { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public string Channel { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public string Status { get; set; }
        
        public decimal Sum { get; set; }
    }
}