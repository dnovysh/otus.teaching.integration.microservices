﻿﻿
using System;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    public class CreditApplicationReadyToSendDto
    {
        public Guid Id { get; set; }
    }
}
