﻿using System;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto
{
    ///<summary>
    /// Данные создания заявки
    /// </summary>
    /// <example>
    ///{
    ///    "customerId": "1fb6f3ef-fd58-4df9-bb29-0bc6f9b57241",
    ///    "channel": 2,
    ///    "Sum": 10000.12
    ///}
    /// </example>
    public class CreateCreditApplicationDto
    {
        public Guid CustomerId { get; set; }
        public AcquisitionChannel Channel { get; set; }
        public decimal Sum { get; set; }
    }
}
