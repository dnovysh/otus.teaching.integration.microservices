﻿using System;
using System.Threading.Tasks;
using System.Linq;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.DataAccess.Repositories.Abstractions;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Domain.Aggregates.CreditApplication;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Dto;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Core.Services;
using Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Integration.Producers;

namespace Otus.Teaching.Integration.Microservices.CreditApplicationAggregator.Host.Application.Services
{
    public class CreditApplicationService : ICreditApplicationService
    {
        private readonly ICreditApplicationRepository _creditApplicationRepository;
        private readonly ICreateNewCreditApplicationProducer _createNewCreditApplicationProducer; 

        public CreditApplicationService(ICreditApplicationRepository creditApplicationRepository, ICreateNewCreditApplicationProducer createNewCreditApplicationProducer)
        {
            _creditApplicationRepository = creditApplicationRepository;
            _createNewCreditApplicationProducer = createNewCreditApplicationProducer;
        }


        public async Task<CreditApplicationsForListDto> GetCreditApplicationsForListAsync()
        {
            var customers = await _creditApplicationRepository.GetAllAsync();

            return new CreditApplicationsForListDto()
            {
                Items = customers.Select(x => new CreditApplicationsForListItemDto()
                {
                    Id = x.Id,
                    Channel = x.Channel.ToString(),
                    Status = x.Status.ToString(),
                    Sum = x.Sum,
                    CreatedDate = x.CreatedDate,
                    CustomerId = x.CustomerId
                }).ToList()
            };
        }

        public async Task<CreditApplicationDto> GetCreditApplicationAsync(Guid id)
        {
            var application = await _creditApplicationRepository.GetByIdAsync(id);

            if(application == null)
                throw new Exception("Application not found");

            return new CreditApplicationDto()
            {
                Id = application.Id,
                Channel = application.Channel,
                Status = application.Status,
                Sum = application.Sum,
                CreatedDate = application.CreatedDate,
                CustomerId = application.CustomerId
            };
        }

        public async Task<CreditApplicationReadyToSendDto> CreateCreditApplicationAsync(CreateCreditApplicationDto creditApplicationDto)
        {
            if (creditApplicationDto == null)
                throw new ArgumentNullException(nameof(creditApplicationDto));

            var customer = new CreditApplication()
            {
                Id = Guid.NewGuid(),
                Channel = creditApplicationDto.Channel,
                CreatedDate = DateTime.Now,
                CustomerId = creditApplicationDto.CustomerId,
                Status = CreditApplicationStatus.New
            };
            
            await _creditApplicationRepository.AddAsync(customer);

            return new CreditApplicationReadyToSendDto()
            {
                Id = customer.Id
            };
        }

        public async Task CreateCreditApplicationByBusAsync(CreateCreditApplicationDto creditApplicationDto)
        {
            await _createNewCreditApplicationProducer.CreateNewCreditApplication(new CreditApplicationImportDto()
            {
                Id = Guid.NewGuid(),
                Channel = creditApplicationDto.Channel.ToString(),
                Status = CreditApplicationStatus.New.ToString(),
                Sum = creditApplicationDto.Sum,
                CreatedDate = DateTime.Now,
                CustomerId = creditApplicationDto.CustomerId
            });
        }

        public async Task EditCreditApplicationAsync(EditCreditApplicationDto creditApplicationDto)
        {
            if (creditApplicationDto == null)
                throw new ArgumentNullException(nameof(creditApplicationDto));

            var application = await _creditApplicationRepository.GetByIdAsync(creditApplicationDto.Id);
            
            if(application == null)
                throw new Exception("Application not found");

            application.Status = creditApplicationDto.Status;

            await _creditApplicationRepository.UpdateAsync(application);
        }
    }
}
